# Docker Compose Integration Test

This is code to accompany the blog post at [http://blog.dupplaw.me.uk/articles/2018-08/integration-test-with-docker](http://blog.dupplaw.me.uk/articles/2018-08/integration-test-with-docker).

## Running the integration test

First ensure that you have gotten and built the service we're testing and
the integration test itself, from:

* [service-under-test](https://gitlab.com/davedupplaw/service-under-test)
* [integration-test](https://gitlab.com/davedupplaw/integration-test)

See the README in each of those on how to build them.

Then run the integration test through docker-compose:

```
docker-compose up --abort-on-container-exit
```

